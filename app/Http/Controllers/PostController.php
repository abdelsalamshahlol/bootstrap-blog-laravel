<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $this->validate($request, [
        'title' => 'required|unique:posts,title|max:80|min:5',
        'editor1' => 'required|max:8000|min:100',
        ],[
        'title.required' => 'This field is required. Please include title.',
        'editor1.required' => 'This field is required. Please write a Post.',
        ]);

       $post = new Post;
       $post->email = "dummy";
       $post->title = $request->title;
       $post->post = $request->editor1;

       $post->save();

       return redirect(url("/"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $data = Post::findOrFail($id);
        //or use try and catch method with findOrFail and place the redirect to catch part

        $data = Post::find($id);
        if(is_null($data))
            {
                return redirect()->to("/");
        }
        else {
            return view('read',['data'=>$data]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        redirect('/');
    }
}
