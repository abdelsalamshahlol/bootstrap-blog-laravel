<div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item @if(Request::segment(1)=="")active @endif" href="{{url("/")}}">Home</a>
          <a class="blog-nav-item @if(Request::segment(1)=="write")active @endif" href="{{url("write")}}">Write Blog</a>
          <a class="blog-nav-item @if(Request::segment(1)=="post_action")active @endif" href="{{route("read",1)}}">Read</a>
          <a class="blog-nav-item @if(Request::segment(1)=="about_us")active @endif" href="{{route("about_us")}}">About</a>
          <div class="blog-nav navbar-right">
          <a class="blog-nav-item" style="color:white;">
          <?php
          use Illuminate\Support\Facades\Auth;
          $user=Auth::user();
          if($user!=""){
          echo "Welcome back ".$user['name'];
          }
          ?>
          </a>

          <?php
          $con=Auth::check();
          if($con){
          echo '<a class="blog-nav-item" href="'. route("logout").'">Logout</a>';
          }
          else{
          echo '<a class="blog-nav-item" href="'. route("login").'">Login</a>';
          }

          ?>
          </div>
        </nav>
      </div>
    </div>
