<div class="container">


        <div class="row">

          <div class="col-sm-8 blog-main">

            <div class="blog-post">
              <h2 class="blog-post-title">{{ $post->title }}</h2>
              <p class="blog-post-meta">{{ $post->created_at }} by <a href="#">{{ $post->email }}</a></p>
                  @php
                  $string = $post->post;
                  $string = strip_tags($string);
                  if (strlen($string) > 500) {
                      $stringCut = substr($string, 0, 500);
                      $num=$post->id;
                      $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '... <a href='. route("read",$num).'>Read More</a>';
                  }
                  echo $string;
                  @endphp

            </div><!-- /.blog-post -->
