@extends('layouts.master')

@section('title','Post')

@section('nav')
    @parent

    {{-- <p>Home nav.</p> --}}
@endsection

@section('content')

<div class="container">
{{-- {{$post->all()}} --}}
      <div class="blog-header">
        <h1 class="blog-title">The Teachy Blog</h1>
        <p class="lead blog-description">Junk MTV quiz graced by fox whelps.</p>
      </div>
      {{-- {{print_r($data)}} --}}
      <div class="row">
          
        <div class="col-sm-8 blog-main">

          <div class="blog-post">
            <h2 class="blog-post-title">{{$data->title}}</h2>
            <p class="blog-post-meta">{{$data->created_at." by"}} <a href="#">{{$data->email}}</a></p>
            <p> {!!$data->post!!} </p>
        </div>
        <nav>
          <ul class="pager">
            <li>
                <form class="" action="{{route("post.destroy",$data->id)}}" method="post">
                                {{ method_field('DELETE') }}
                                {{CSRF_field()}}
                <input type="hidden" name="id" value="{{$data->id}}">
                <button class="btn btn-danger">Delete Post</button>
            </form>
            </li>
            <li><a href="{{route("read",$data->id-1)}}">Previous</a></li>
            <li><a href="{{route("read",$data->id+1)}}">Next</a></li>
          </ul>
        </nav>
    </div>
{{-- </div> --}}
{{-- </div> --}}

@endsection

@section('sidebar')
    {{-- <p>This is my sidebar.</p> --}}

@endsection

@section('footer')

@endsection
