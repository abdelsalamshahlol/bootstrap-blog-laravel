@extends('layouts.master')

@section('title','Post')

@section('nav')
    @parent

    {{-- <p>Home nav.</p> --}}
@endsection

@section('content')
<script src="//cdn.ckeditor.com/4.6.2/basic/ckeditor.js"></script>

<div id='a1' style="position: absolute; display: none;" class="alert alert-danger text-center col-md-12">
    @if (count($errors) > 0)
        <div class="container">

                @foreach ($errors->all() as $error)
                    {{ $error }}<br>
                @endforeach

        </div>
    @endif
</div>

<div class="container">

    <div class="blog-header">
      <h1 class="blog-title">The Teachy Blog</h1>
      <p class="lead blog-description">Junk MTV quiz graced by fox whelps.</p>
    </div>

      <div class="row">

        <div class="col-sm-8 blog-main">

          <div class="blog-post">

            <h2 class="blog-post-title">Write a post:</h2>
    <div class="row">
        <div class="col-md-6 col-md-offest-4">
                <form method="post" action="{{route("post.store")}}" name="userpost">
                    {{CSRF_field()}}
                    <div  style="display: none" id="text-editor" class="row">
                        <div class="form-group row col-md-8 col-md-offset-2">
                            <label class=" control-label" for="title">Title:</label>
                            <input  name="title" type="text" placeholder="" class="form-control input-md">
                        </div>
                        <div class="form-group">
                            <div class="container col-md-8 col-md-offset-2">
                                <label class=" control-label">Post:</label>
                                <textarea class="hidden" name="editor1"></textarea>
                                <br>
                                <button id="btn1" class="btn btn-primary">Publish Post</button>
                                <a href="{{url("/")}}"><input type="button" value="Cancel" id="btn2" class="btn btn-default"></a>
                            </div>
                        </div>
                    </div>
                </form>
         </div>
     </div>
        </div>
    </div>

<script>
    $(document).ready(function () {
        CKEDITOR.replace('editor1', {
          width:['60%'],uiColor: '#428bca',

            on: {
                instanceReady: function (evt) {
                    $("#text-editor").fadeIn("slow");
                }
            }
        });

        var error ={{count($errors)}};
        if (parseInt(error) > 0) {
            $("#a1").show();
            function toggleDiv() {
                setTimeout(function () {
                    $("#a1").fadeOut("slow");
                }, 5000);
            }
            toggleDiv();
        }
    }
    );

</script>
@endsection

@section('sidebar')
    {{-- <p>This is my sidebar.</p> --}}

@endsection

@section('footer')

@endsection
