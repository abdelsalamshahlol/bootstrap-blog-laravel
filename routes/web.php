<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Post;

Route::get('/', function () {
  $data=Post::all();
    return view('home',['data'=>$data]);
    // return view('home');
});

Route::get('about_us', function()
{
  $d="message";
    return view('aboutus')->with('test',$d);
})->name('about_us');

Route::get('write', function()
{
    return view('write');
})->middleware('auth');

// Route::get('/read/{post?}', function($postId='null')
// {
//     // return 'Reading post number '.$postId;
//     return view('read',['postId'=>$postId]);
// })->where(['post'=>'[0-9]{0,}'],[])->name('read');

// Route::get('login', function()
// {
//     return view('login');
// })->name("user_login");

Route::resource('post', 'PostController',['names'=>[
  'show'=>'read'
  ]]);


// Route::match(['get','patch'],'test', function()
// {
//     return 'TEST';
// });

// Route::any('/admin', function()
// {
//     return abort(404, 'Page not found');
// });

//this is a route to logout method in the login controller
//you shouldn't use the get method because it will logout other application
//other hack its stupid hidden form in ur logiut page with csrf field
Route::get('/logout', 'Auth\LoginController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
